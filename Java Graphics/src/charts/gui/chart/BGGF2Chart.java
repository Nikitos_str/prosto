package charts.gui.chart;

import charts.lib.BGStackedChartData;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.math.MathContext;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGGF2Chart extends BGStackedChart{

    BGStackedChartData data;

    public BGGF2Chart(BGStackedChartData data){
        this.data = data;
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);

        drawData();
        drawLineLegennd();
    }

    private void drawData(){
        for(int i = 1; i<data.getSize(); i++){
            if((data.get(i) != null )&&(data.get(i-1) != null)){
                Point2D point1 = data.get(i);
                Point2D point2 = data.get(i - 1);
                drawLine((Point2D.Float) point1, (Point2D.Float) point2, data.getSize(), i);
            }
        }
    }

    private void drawLineLegennd(){
        Color oldColor = g2d.getColor();
        FontMetrics fm = getFontMetrics(getFont());
        int count = data.getSize();

        for(int i = 0; i < count; i++){
            float posX1 = (getWidth() - 2 * DEFAULT_PADDING) / count * i;
            int string_width_X = fm.stringWidth(String.valueOf(data.getMaxX()));
            int string_width_Y = fm.stringWidth(String.valueOf(data.getMaxY()));
            int string_width_0 = fm.stringWidth("0");
            String strX = String.valueOf(data.get(i).x);
            String strY = String.valueOf(data.get(i).y);

            Line2D line = new Line2D.Float(
                    posX1 + DEFAULT_PADDING,
                    DEFAULT_PADDING,
                    posX1 + DEFAULT_PADDING,
                    getHeight() - DEFAULT_PADDING
            );

            g2d.setColor(Color.LIGHT_GRAY);
            g2d.draw(line);
            g2d.drawString(
                    "0",
                    DEFAULT_PADDING - string_width_0 * 2,
                    getHeight()/2 + getFont().getSize() / 2
            );

            g2d.setColor(Color.BLACK);
            g2d.drawString(
                    strX,
                    posX1 + DEFAULT_PADDING - string_width_X / 2,
                    getHeight() - DEFAULT_PADDING + getFont().getSize() * 2
            );
            g2d.drawString(
                    strY,
                    posX1 + DEFAULT_PADDING - string_width_Y / 2,
                    getHeight() - DEFAULT_PADDING + getFont().getSize()
            );
        }

        g2d.setColor(oldColor);
    }

    private void drawLine(Point2D.Float point1, Point2D.Float point2, int num, int count){
        Color oldColor = g2d.getColor();

        float posX1 = (getWidth() - 2 * DEFAULT_PADDING) / num * count;
        float posX2 = (getWidth() - 2 * DEFAULT_PADDING) / num * (count - 1);

        float maxValue = Math.max(Math.abs(data.getMaxY()), Math.abs(data.getMinY()));
        float relY = (getHeight()/2 - DEFAULT_PADDING) / maxValue;

        float yMiddle = getHeight()/2;

        Line2D line = new Line2D.Float(
                posX1 + DEFAULT_PADDING,
                yMiddle - point1.y * relY,
                posX2 + DEFAULT_PADDING,
                yMiddle - point2.y * relY
        );

        g2d.setColor(Color.BLACK);
        g2d.draw(line);

        g2d.setColor(oldColor);
    }
}
