package charts.gui.chart;

import charts.lib.BGDimension;
import charts.lib.BGTDData;
import com.sun.prism.paint.*;
import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Vector;

/**
 * Created by Nikita on 19.12.15.
 */
public class BGTDChart extends BGChart {

    BGTDData data;

    public BGTDChart(BGTDData data){
        this.data = data;
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        drawAxis();
        drawStr();
        drawData();
    }

    private void drawStr(){
        Color oldColor = g2d.getColor();

        FontMetrics fm = getFontMetrics(getFont());
        int string_width_X = fm.stringWidth(String.valueOf(getXMax()));
        int string_width_Y = fm.stringWidth(String.valueOf(getYMax()));

        g2d.setColor(Color.BLACK);
        g2d.drawString(
                String.valueOf(getYMax()),
                getWidth()/2 - string_width_Y/2,
                DEFAULT_PADDING - getFont().getSize() / 2
        );
        g2d.drawString(
                "-" + String.valueOf(getYMax()),
                getWidth()/2 - string_width_Y,
                getHeight() - DEFAULT_PADDING + getFont().getSize()
        );
        g2d.drawString(
                String.valueOf(getXMax()),
                getWidth() - DEFAULT_PADDING + string_width_X/2,
                getHeight() / 2  + getFont().getSize() / 2
        );
        g2d.drawString(
                "-" + String.valueOf(getXMax()),
                DEFAULT_PADDING - string_width_X * 2,
                getHeight() / 2  + getFont().getSize() / 2
        );

        g2d.setColor(oldColor);
    }


    private void drawAxis(){
        Color oldColor = g2d.getColor();
        g2d.setColor(Color.BLACK);
        Line2D axisX = new Line2D.Double(
                DEFAULT_PADDING,
                getHeight()/2,
                getWidth() - DEFAULT_PADDING,
                getHeight()/2
        );
        Line2D axisY = new Line2D.Double(
                getWidth()/2,
                DEFAULT_PADDING,
                getWidth()/2,
                getHeight() - DEFAULT_PADDING
        );
        g2d.draw(axisX);
        g2d.draw(axisY);
        g2d.setColor(oldColor);
    }

    private void  drawData(){
        for(int i =0; i< data.getSize(); i++){

            Point2D.Float point = data.get(i);
            if(point != null){
                drawPoint(point, data.getColor(i));
            }
        }
    }


    private void drawPoint(Point2D.Float point, Color color){
        Color oldColor = g2d.getColor();
        float yMiddle = getHeight() / 2;
        float xMiddle = getWidth() / 2;
        float relX = (xMiddle - DEFAULT_PADDING) / getXMax();
        float relY = (yMiddle - DEFAULT_PADDING) / getYMax();
        int radius = 3;

        Ellipse2D ellipse = new Ellipse2D.Double(
                xMiddle + point.getX() * relX - radius,
                yMiddle - point.getY() * relY - radius,
                2 * radius,
                2 * radius
        );

        g2d.setColor(color);
        g2d.fill(ellipse);
        g2d.draw(ellipse);

        g2d.setColor(oldColor);
    }

    private int getXMax() {
        return (((int) Math.abs(data.getMaxX()) / 10 + 1) * 10);
    }

    private int getYMax() {
        return (((int) Math.abs(data.getMaxY()) / 10 + 1) * 10);
    }
}
