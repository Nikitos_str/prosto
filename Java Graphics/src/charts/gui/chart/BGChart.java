package charts.gui.chart;

import charts.lib.BGDimension;
import com.sun.prism.paint.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGChart extends JComponent{

    protected int DEFAULT_HEIGHT = BGDimension.SCREEN_SIZE.height / 2;
    protected int DEFAULT_WIDTH = BGDimension.SCREEN_SIZE.width / 2;
    protected int DEFAULT_PADDING = 50;
    Graphics2D g2d;

    public BGChart(){

    }


    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        drawBox();
    }

    protected void drawBox(){
        Color oldColor = g2d.getColor();
        g2d.setColor(Color.LIGHT_GRAY);
        Line2D southLine = new Line2D.Double(
                DEFAULT_PADDING,
                DEFAULT_PADDING,
                getWidth() - DEFAULT_PADDING,
                DEFAULT_PADDING
        );
        Line2D northLine = new Line2D.Double(
                DEFAULT_PADDING,
                getHeight() - DEFAULT_PADDING,
                getWidth() - DEFAULT_PADDING,
                getHeight() - DEFAULT_PADDING
        );
        Line2D eastLine = new Line2D.Double(
                DEFAULT_PADDING,
                DEFAULT_PADDING,
                DEFAULT_PADDING,
                getHeight() - DEFAULT_PADDING
        );
        Line2D westLine = new Line2D.Double(
                getWidth() - DEFAULT_PADDING,
                DEFAULT_PADDING,
                getWidth() - DEFAULT_PADDING,
                getHeight() - DEFAULT_PADDING
        );

        g2d.draw(southLine);
        g2d.draw(northLine);
        g2d.draw(westLine);
        g2d.draw(eastLine);

        g2d.setColor(oldColor);
    }

    public Dimension getPreferredSize() {
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public Point2D getCenter(){
        return new Point2D.Double(getWidth()/2, getHeight()/2);
    }
}
