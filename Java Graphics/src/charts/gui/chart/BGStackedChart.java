package charts.gui.chart;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created by Nikita on 19.12.15.
 */
public class BGStackedChart extends BGChart {

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        drawAxis();
    }

    private void drawAxis(){
        Color oldColor = g2d.getColor();

        Line2D lineY = new Line2D.Float(
                DEFAULT_PADDING,
                DEFAULT_PADDING,
                DEFAULT_PADDING,
                getHeight() - DEFAULT_PADDING
        );
        Line2D lineX = new Line2D.Float(
                DEFAULT_PADDING,
                getHeight() / 2,
                getWidth() - DEFAULT_PADDING,
                getHeight() / 2
        );

        g2d.setColor(Color.BLACK);
        g2d.draw(lineX);
        g2d.draw(lineY);

        g2d.setColor(oldColor);
    }
}
