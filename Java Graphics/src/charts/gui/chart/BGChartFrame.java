package charts.gui.chart;

import charts.lib.BGDimension;

import javax.swing.*;
import java.awt.*;

public class BGChartFrame extends JFrame {

    public BGChartFrame(BGChart bgChart){
        this.add(bgChart);
        setUpAndShow();
    }

    private void setUpAndShow() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(BGDimension.CHART_FRAME_SIZE);
        setPreferredSize(BGDimension.CHART_FRAME_SIZE);
        setSize(new Dimension(
                (int) (BGDimension.SCREEN_SIZE.width * 0.6),
                (int) (BGDimension.SCREEN_SIZE.height * 0.6)
        ));
        setTitle("Chart");
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
