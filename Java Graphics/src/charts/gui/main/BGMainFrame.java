package charts.gui.main;

import charts.gui.main.buttons.*;
import charts.lib.BGDefaultTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGMainFrame extends JFrame {

    JTable table = new JTable(BGDefaultTableModel.getModel());

    public BGMainFrame(){
        this.setLayout(new BorderLayout());
        this.add(chartButton(), BorderLayout.NORTH);
        this.add(tablePanel(), BorderLayout.CENTER);
        this.add(tableButton(), BorderLayout.SOUTH);

        setUpAndShow();
    }

    private JPanel chartButton(){
        JPanel panel = new JPanel();
        JButton gf2 = new BGGF2ChartButton("GF2", (DefaultTableModel) table.getModel());
        JButton td = new BGTDChartButton("TD", (DefaultTableModel) table.getModel());

        panel.add(gf2);
        panel.add(td);

        return panel;
    }

    private JPanel tableButton(){
        JPanel panel = new JPanel();
        JButton addButton = new BGAddRowControlButton(table);
        JButton deleteButton = new BGDeleteRowControlButton(table);

        panel.add(addButton);
        panel.add(deleteButton);

        return panel;
    }

    private JPanel tablePanel(){
        JPanel panel = new JPanel();
        Dimension tableSize = new Dimension(220, 350);

        table.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        table.setAutoResizeMode(JTable.WIDTH);
        table.setPreferredScrollableViewportSize(tableSize);


        JScrollPane scrollPane = new JScrollPane(
                table,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
        );

        scrollPane.setMaximumSize(tableSize);
        panel.add(scrollPane);
        return panel;
    }

    private void setUpAndShow(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.setTitle("Table");
        this.setMinimumSize(new Dimension((int) 220, (int) (screenSize.height * 0.6)));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

    }
}
