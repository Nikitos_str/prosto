package charts.gui.main.buttons;

import charts.lib.BGDimension;

import javax.swing.*;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGAbstractButton extends JButton{

    public BGAbstractButton(String str){
        super(str);
        setMaximumSize(BGDimension.BUTTON_SIZE);
        setMinimumSize(BGDimension.BUTTON_SIZE);
        setPreferredSize(BGDimension.BUTTON_SIZE);
    }

}
