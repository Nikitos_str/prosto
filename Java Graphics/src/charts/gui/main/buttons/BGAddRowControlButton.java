package charts.gui.main.buttons;

import charts.lib.BGDefaultTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGAddRowControlButton extends BGControlButton{

    public BGAddRowControlButton(JTable table) {
        super("+", table);
        this.addActionListener(new Listener());
    }

    private class Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            model.addRow(BGDefaultTableModel.getEmptyVector(model.getColumnCount()));
        }
    }
}

