package charts.gui.main.buttons;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGChartButton extends BGAbstractButton {

    DefaultTableModel model;

    public BGChartButton(String str, DefaultTableModel model) {
        super(str);
        this.model = model;
    }
}
