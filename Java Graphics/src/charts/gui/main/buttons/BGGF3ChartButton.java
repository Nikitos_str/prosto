package charts.gui.main.buttons;

import charts.gui.chart.BGChart;
import charts.gui.chart.BGChartFrame;
import charts.gui.chart.BGGF3Chart;
import charts.lib.BGStackedChartData;

import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGGF3ChartButton extends BGChartButton {

    public BGGF3ChartButton(String str, DefaultTableModel model) {
        super(str, model);
        this.addActionListener(new Listener());
    }

    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            BGStackedChartData data = new BGStackedChartData(model);
            BGGF3Chart chart = new BGGF3Chart(data);
            new BGChartFrame(chart);
        }
    }
}
