package charts.gui.main.buttons;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGControlButton extends BGAbstractButton {

    protected JTable table;

    public BGControlButton(String str, JTable table) {
        super(str);
        this.table = table;
    }
}
