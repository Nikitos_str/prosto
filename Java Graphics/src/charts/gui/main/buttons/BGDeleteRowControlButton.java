package charts.gui.main.buttons;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGDeleteRowControlButton extends BGControlButton {

    public BGDeleteRowControlButton(JTable table) {
        super("-", table);
        addActionListener(new Listener());
    }

    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            if (model.getRowCount() == 0) {
                JOptionPane.showMessageDialog(
                        null,
                        "Table is empty!"
                );
            } else model.removeRow(model.getRowCount() - 1);
        }
    }

}
