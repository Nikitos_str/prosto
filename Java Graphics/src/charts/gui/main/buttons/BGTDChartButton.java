package charts.gui.main.buttons;

import charts.gui.chart.BGChart;
import charts.gui.chart.BGChartFrame;
import charts.gui.chart.BGTDChart;
import charts.lib.BGTDData;

import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGTDChartButton extends BGChartButton {

    public BGTDChartButton(String str, DefaultTableModel model) {
        super(str, model);
        this.addActionListener(new Listener());
    }

    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            BGTDData data = new BGTDData(model);
            BGTDChart chart = new BGTDChart(data);
            new BGChartFrame(chart);
        }
    }
}
