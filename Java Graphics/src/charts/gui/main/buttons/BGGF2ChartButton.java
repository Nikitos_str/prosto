package charts.gui.main.buttons;

import charts.gui.chart.BGChart;
import charts.gui.chart.BGChartFrame;
import charts.gui.chart.BGGF2Chart;
import charts.gui.chart.BGStackedChart;
import charts.lib.BGStackedChartData;

import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGGF2ChartButton extends BGChartButton {
    public BGGF2ChartButton(String str, DefaultTableModel model) {
        super(str, model);
        this.addActionListener(new Listener());
    }

    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            BGStackedChartData data = new BGStackedChartData(model);
            BGGF2Chart chart = new BGGF2Chart(data);
            new BGChartFrame(chart);
        }
    }
}
