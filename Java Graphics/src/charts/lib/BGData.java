package charts.lib;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Vector;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGData {
    /**
     * Stores colors for each row
     */
    private Vector<Color> color;

    /**
     * Creates base data object
     * @param model - default table model from table
     */
    public BGData(DefaultTableModel model) {
        prepareColors(model.getRowCount());
    }

    /**
     * Generates color vector for painting
     * @param count - amount of columns
     */
    private void prepareColors(int count) {
        color = new Vector<>(count);

        for (int i = 0; i < count; i++)
            color.add(BGColor.getRandomColor());
    }

    /**
     * Returns color for table row
     * @param count - row number
     * @return Color
     */
    public Color getColor(int count) {
        return color.get(count);
    }
}
