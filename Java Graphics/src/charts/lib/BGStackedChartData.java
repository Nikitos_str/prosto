package charts.lib;

import javax.swing.table.DefaultTableModel;
import java.awt.geom.Point2D;
import java.util.Vector;

/**
 * Created by Nikita on 19.12.15.
 */
public class BGStackedChartData extends BGData {

    /**
     * Maximum X-coordinate
     */
    private float maxX;

    /**
     * Maximum Y-coordinate
     */
    private float maxY;

    /**
     * Minimum Y-coordinate;
     */
    private float minY;

    /**
     * Stores all available points extracted from def. table model
     */
    private Vector<Point2D.Float> data;

    /**
     * Creates data object for TD chart
     *
     * @param model - def. table model from table
     */
    public BGStackedChartData(DefaultTableModel model) {
        super(model);

        prepareData(model.getDataVector());
    }

    /**
     * Prepare data for data
     *
     * @param vector vector from TabelModel
     */
    public void prepareData(Vector vector) {
        this.data = new Vector<>();
        Vector tmp_vector;

        for (int i = 0; i < vector.size(); i++) {
            try {

                tmp_vector = (Vector) vector.get(i);

                float tmp_x = Float.parseFloat((String) tmp_vector.get(0));
                float tmp_y = Float.parseFloat((String) tmp_vector.get(1));

                Point2D point = new Point2D.Float(tmp_x, tmp_y);

                if (point != null) {
                    data.add((Point2D.Float) point);
                }
            } catch (Exception e) {
                //
            }
        }

        sortingData();
        prepareSumY();
        prepareMaxValue();
    }

    /**
     * Sorting data
     */
    private void sortingData(){
        Point2D point_tmp;
        for(int i = 0; i < data.size(); i++){
            for (int j =0; j<data.size(); j++){
                if(data.get(i).x < data.get(j).x) {
                    point_tmp = data.get(i);
                    data.set(i, data.get(j));
                    data.set(j, (Point2D.Float) point_tmp);
                }
            }
        }
    }

    /**
     * Prepare sum of Y-coordinate
     */
    private void prepareSumY(){
        float sumY = data.get(0).y;
        for(int i =1; i<data.size(); i++){
            sumY += data.get(i).y;
            data.get(i).y = sumY;
        }
    }

    /**
     * Searsh maximum and minimum of X-coordinate and Y-Coordinate
     */
    public void prepareMaxValue(){
        if(data.isEmpty())
            return;

        maxX = data.get(0).x;
        maxY = data.get(0).y;
        minY = data.get(0).y;

        for(int i = 0; i < data.size(); i++){
            float tmp_float_X = data.get(i).x;
            if(tmp_float_X > maxX)
                maxX = tmp_float_X;

            float tmp_float_Y = data.get(i).y;
            if(tmp_float_Y > maxY)
                maxY = tmp_float_Y;
            if(tmp_float_Y < minY)
                minY = tmp_float_Y;
        }
    }

    /**
     * Return maximum value of X-coordinate
     * @return maxX
     */
    public float getMaxX(){
        return maxX;
    }

    /**
     * Return maximum value of Y-coordinate
     * @return maxY
     */
    public float getMaxY(){
        return maxY;
    }

    /**
     * Return minimum value of Y-coordinate
     * @return minY
     */
    public float getMinY(){
        return  minY;
    }

    /**
     * Return point2D.Float from data
     * @return Point2D.Floar
     */
    public Point2D.Float get(int i){
        return data.get(i);
    }

    /**
     * Return count of Points
     * @return int count
     */
    public int getSize(){
        return data.size();
    }
}
