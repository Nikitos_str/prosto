package charts.lib;

import javax.swing.table.DefaultTableModel;
import java.awt.geom.Point2D;
import java.security.PublicKey;
import java.util.Vector;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGTDData extends BGData {
    /**
     * Biggest x-coordinate
     */
    private float maxX;

    /**
     * Biggest y-coordinate
     */
    private float maxY;

    /**
     * Stores all available points extracted from def. table model
     */
    private Vector<Point2D.Float> data;

    /**
     * Creates data object for TD chart
     * @param model - def. table model from table
     */
    public BGTDData(DefaultTableModel model) {
        super(model);
        prepareData(model.getDataVector());
        prepareMaxValue();
    }

    /**
     * Extracts data from data vector from def. table model
     * @param vector to main data stores
     */
    public void prepareData(Vector vector){
        this.data = new Vector<>();
        Vector tmp_vector;

        for(int i = 0; i<vector.size(); i++){
            try {

                tmp_vector = (Vector) vector.get(i);

                float tmp_x = Float.parseFloat((String) tmp_vector.get(0));
                float tmp_y = Float.parseFloat((String) tmp_vector.get(1));

                Point2D point = new Point2D.Float(tmp_x, tmp_y);

                if(point != null) data.add((Point2D.Float) point);
            }
            catch (Exception e){
                //
            }
        }
    }

    /**
     * Search maximum value of X-coordinate and Y-coordinate
     */
    public void prepareMaxValue(){
        if(data.size() !=0 ){
            maxX = data.get(0).x;
            maxY = data.get(0).y;

            for(int i = 0; i<data.size(); i++){
                float tmp_float_X = data.get(i).x;
                float tmp_float_Y = data.get(i).y;

                if(tmp_float_X > maxX)
                    maxX = tmp_float_X;
                if(tmp_float_Y > maxY)
                    maxY = tmp_float_Y;
            }
        }
    }

    /**
     * Returns maximum of X-coordinate
     * @return maxX
     */
    public float getMaxX(){
        return maxX;
    }

    /**
     * Returns maximum of Y-coordinate
     * @return maxY
     */
    public float getMaxY(){
        return maxY;
    }

    /**
     * Counts of points
     * @return data.size()
     */
    public int getSize(){
        return data.size();
    }

    /**
     * Returns current point from stores
     * @param i - number point
     * @return current point
     */
    public Point2D.Float get(int i) {
        return data.get(i);
    }
}