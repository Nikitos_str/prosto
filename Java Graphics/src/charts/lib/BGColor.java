package charts.lib;

import java.awt.*;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGColor {
    /**
     * Returns random color
     * @return randomColor
     */
    public static Color getRandomColor() {
        return new Color(
                (int)(Math.random() * 255),
                (int)(Math.random() * 255),
                (int)(Math.random() * 255)
        );
    }
}
