package charts.lib;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGDefaultTableModel {

    /**
     * Creates table model
     * @return DefaultTableModel
     */
    public static DefaultTableModel getModel() {
        String colNames[] = {
                "X",
                "Y",
        };

        Object data[][] = {
                {null, null, null}
        };

        return new DefaultTableModel(data, colNames);
    }

    /**
     * Returns new empty vector
     * @param size - size of new empty vector
     * @return new empty vector
     */
    public static Vector getEmptyVector(int size) {
        Vector<Object> vector = new Vector<>(size);

        for (int i = 0; i < size; i++)
            vector.add(null);

        return vector;
    }

}
