package charts.lib;

import java.awt.*;

/**
 * Created by Nikita on 18.12.15.
 */
public class BGDimension {
    /**
     * Default button size
     */
    public static final Dimension BUTTON_SIZE = new Dimension(60, 30);

    /**
     * Default chart frame size
     */
    public static final Dimension CHART_FRAME_SIZE = new Dimension(300, 300);

    /**
     * Default screen size
     */
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
}
